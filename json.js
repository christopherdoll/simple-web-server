const http = require("http");

const host = 'localhost';
const port = 8000;

// we want to return a JSON response, so modify the requestListener() to return
// the appropriate header all JSON responses have.

// the res.setHeader() method adds an HTTP header to the response.  HTTP headers are
// additional information that can be attached to a request or a response.

// the res.setHeader() method takes two arguments: header's name and its value.

// the "Content-Type" header is used to indicate the format (media type) of the
// data that is being sent with the request/response. in this case our content-type
// is application/json
const requestListener = function (req, res) {
     res.setHeader("Content-Type", "application/json");
      res.writeHead(200);
      res.end('{"message": "This is a JSON response"}');
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log('Server is running on http://${host}:${port}');
});
