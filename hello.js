// http module contains the function to create the server
const http = require("http");
// define host and port the server will be bound to
const host = 'localhost';
const port = 8000;

// requestlistener function is meant to handle an incoming http request and return an http response
// the http request the user sends is captured in a request object, which is the first argument req
// even though requestListener() does not use the req object, it still must be the first arg of the function
// the http response is formed by interacting with the response  object in the second arg res
const requestListener = function (req, res) {
    res.writeHead(200);
    res.end("My first server!");
};

// this creates a new server object via the http module's createServer function
// the server accepts HTTP requests and passes them on to our requestListener function
// you bind the server to a network address using server.listen()
// it accepts three args: port, host, and a callback function that fires when the server begins to listen
// the callback function logs a message to our console so we can know when the server began listening
const server = http.createServer(requestListener);
server.listen(port, host, () => {
    console.log(`Server is running on http://${host}:${port}`);
});
